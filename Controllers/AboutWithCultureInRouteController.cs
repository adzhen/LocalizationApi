﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Localization_APIDemo.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Localization_APIDemo.Controllers
{
    [ServiceFilter(typeof(LanguageActionFilter))]
    [Route("api/{culture}/[controller]")]
    public class AboutWithCultureInRouteController : Controller
    {
        
        private readonly IStringLocalizer<SharedResource> _localizer;

        public AboutWithCultureInRouteController(IStringLocalizer<SharedResource> localizer)
        {
            _localizer = localizer;
        }

        [HttpGet]
        public string Get()
        {
            return _localizer["Name"];
        }

        [HttpGet]
        [Route("CheckCulture")]
        public string CheckCulture()
        {
            return "YES";
        }
    }
}