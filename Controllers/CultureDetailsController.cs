﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Localization.SqlLocalizer.DbStringLocalizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace Localization_APIDemo.Controllers
{
    [Route("api/[controller]")]
    public class CultureDetailsController : Controller
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<CultureDetailsController> _aboutLocalizerizer;

        public CultureDetailsController(IStringLocalizer<SharedResource> localizer, IStringLocalizer<CultureDetailsController> aboutLocalizerizer)
        {
            _localizer = localizer;
            _aboutLocalizerizer = aboutLocalizerizer;
        }

        [HttpGet]
        public string Get(string culture)
        {
            return _aboutLocalizerizer["AboutTitle"];
        }
    }
}